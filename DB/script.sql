/****** Object:  UserDefinedTableType [dbo].[CurrencyInfo]    Script Date: 30.03.2021 7:55:37 ******/
CREATE TYPE [dbo].[CurrencyInfo] AS TABLE(
	[ID] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[EngName] [nvarchar](50) NULL,
	[Nominal] [int] NULL,
	[ParentCode] [nvarchar](50) NULL,
	[ISO_Num_Code] [smallint] NULL,
	[ISO_Char_Code] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[CurrencyRateInfo]    Script Date: 30.03.2021 7:55:37 ******/
CREATE TYPE [dbo].[CurrencyRateInfo] AS TABLE(
	[ID] [nvarchar](50) NULL,
	[Value] [numeric](18, 4) NULL,
	[Date] [datetime] NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrencyRate]    Script Date: 30.03.2021 7:55:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 30.03.2021 7:55:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[EngName] [nvarchar](50) NOT NULL,
	[Nominal] [int] NOT NULL,
	[ParentCode] [nvarchar](50) NOT NULL,
	[ISO_Num_Code] [smallint] NULL,
	[ISO_Char_Code] [nvarchar](50) NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CurrencyRate]    Script Date: 30.03.2021 7:55:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyRate](
	[ID] [nvarchar](50) NOT NULL,
	[Value] [numeric](18, 4) NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_CurrencyRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD  CONSTRAINT [FK_Currency_CurrencyRate] FOREIGN KEY([ID])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_Currency_CurrencyRate]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertCurrency]    Script Date: 30.03.2021 7:55:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertCurrency]
(
	@Items	dbo.CurrencyInfo READONLY
)

AS

MERGE Currency AS Target
USING @Items AS Source
ON (Target.ID = Source.ID)
WHEN MATCHED THEN
	UPDATE SET
		Name = Source.Name,
		EngName = Source.EngName,
		Nominal = Source.Nominal,
		ParentCode = Source.ParentCode,
		ISO_Num_Code = Source.ISO_Num_Code,
		ISO_Char_Code = Source.ISO_Char_Code
WHEN NOT MATCHED THEN
	INSERT (ID, Name, EngName, Nominal, ParentCode, ISO_Num_Code, ISO_Char_Code)
	VALUES (Source.ID, Source.Name, Source.EngName, Source.Nominal, Source.ParentCode, Source.ISO_Num_Code, Source.ISO_Char_Code)
;

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertCurrencyRate]    Script Date: 30.03.2021 7:55:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertCurrencyRate]
(
	@Items	dbo.CurrencyRateInfo READONLY
)

AS

MERGE CurrencyRate AS Target
USING @Items AS Source
ON (Target.ID = Source.ID AND Target.Date = Source.Date)
WHEN NOT MATCHED THEN
	INSERT (ID, Value, Date)
	VALUES (Source.ID, Source.Value, Source.Date)
;

GO

CREATE FUNCTION [dbo].[fn_GetCurrencyRate]
(
	@currencyId nvarchar(50),
	@currencyDate datetime
)
RETURNS numeric(18, 4)
AS
BEGIN
	DECLARE @Result numeric(18, 4)

	SET @Result = (SELECT [Value] FROM [dbo].[CurrencyRate] WHERE ID = @currencyId AND [Date] = @currencyDate)

	RETURN ISNULL(@Result, 0)
END
