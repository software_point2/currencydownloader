﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CurrencyDownloader
{
    class Program
    {
        static readonly string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        
        static readonly string urlCurrency = "http://www.cbr.ru/scripts/XML_valFull.asp";
        
        static readonly string urlCurrencyRate = "http://www.cbr.ru/scripts/XML_daily.asp?date_req={0}";
        
        static void Main(string[] args)
        {
            LoadCurrencies();
            
            DateTime date = DateTime.Today;
            if (args.Length > 0)
            {
                DateTime.TryParse(args[0], out date);
            }
            LoadCurrencyRates(date);
        }

        //получаем справочник валют
        static void LoadCurrencies()
        {
            var tableCurrency = new TableTypes().CurrencyInfo;
            try
            {
                DataSet ds = new DataSet();
                ds.ReadXml(urlCurrency);
                DataTable currency = ds.Tables["Item"];

                foreach (DataRow row in currency.Rows)
                {
                    var newRow = tableCurrency.NewCurrencyInfoRow();
                    newRow.ID = row["ID"].ToString();
                    newRow.Name = row["Name"].ToString();
                    newRow.EngName = row["EngName"].ToString();
                    newRow.Nominal = int.Parse(row["Nominal"].ToString());
                    newRow.ParentCode = row["ParentCode"].ToString();
                    if (!string.IsNullOrWhiteSpace(row["ISO_Num_Code"].ToString()))
                    {
                        newRow.ISO_Num_Code = int.Parse(row["ISO_Num_Code"].ToString());
                    }
                    newRow.ISO_Char_Code = row["ISO_Char_Code"].ToString();
                    tableCurrency.Rows.Add(newRow);
                }
                SaveCurrencies(tableCurrency);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка получения справочника валют. " + ex.Message);
            }
        }

        //записываем справочник в БД
        static void SaveCurrencies(TableTypes.CurrencyInfoDataTable currencies)
        {
            try
            {
                SqlHelper.ExecuteSP("sp_InsertCurrency", currencies, connectionString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка сохранения справочника валют. " + ex.Message);
            }
        }

        //получаем курсы валют 
        static void LoadCurrencyRates(DateTime date)
        {
            var tableCurrencyRate = new TableTypes().CurrencyRateInfo;
            try
            {
                DataSet ds = new DataSet();
                ds.ReadXml(string.Format(urlCurrencyRate, date.ToString("dd.MM.yyyy")));
                DataTable currencyRate = ds.Tables["Valute"];

                foreach (DataRow row in currencyRate.Rows)
                {
                    var newRow = tableCurrencyRate.NewCurrencyRateInfoRow();
                    newRow.ID = row["ID"].ToString();
                    newRow.Value = decimal.Parse(row["Value"].ToString()
                        .Replace(",", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)
                        .Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                    newRow.Date = date;
                    tableCurrencyRate.Rows.Add(newRow);
                }
                SaveCurrencyRates(tableCurrencyRate);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка получения курсов валют. " + ex.Message);
            }            
        }

        //записываем курсы в БД
        static void SaveCurrencyRates(TableTypes.CurrencyRateInfoDataTable currencyRates)
        {
            try
            {
                SqlHelper.ExecuteSP("sp_InsertCurrencyRate", currencyRates, connectionString);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка сохранения курсов валют. " + ex.Message);
            }
        }
    }
}
