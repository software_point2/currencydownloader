﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyDownloader
{
    static class CurrencyRateHelper
    {
        public static decimal GetCurrencyRate(string currencyId, DateTime date, string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand("select dbo.fn_GetCurrencyRate(@currencyId, @currencyDate)", connection);
                command.Parameters.AddWithValue("@currencyId", currencyId);
                command.Parameters.AddWithValue("@currencyDate", date);
                var dt = new DataTable();
                new SqlDataAdapter(command).Fill(dt);

                return (decimal)dt.Rows[0][0];
            }
        }
    }
}
