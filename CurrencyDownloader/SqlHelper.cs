﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrencyDownloader
{
    static class SqlHelper
    {
        public static void ExecuteSP(string spName, DataTable dataTable, string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                try
                {
                    SqlCommand command = new SqlCommand(spName, connection);
                    command.CommandType = CommandType.StoredProcedure;
                    var param = command.Parameters.AddWithValue("@Items", dataTable);
                    param.TypeName = "dbo." + dataTable.TableName;
                    param.SqlDbType = SqlDbType.Structured;
                    command.ExecuteNonQuery();
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}
